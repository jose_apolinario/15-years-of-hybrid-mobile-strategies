This template was based on the work of Ryan Schuetzler

https://www.schuetzler.net/blog/latex-icis-template/ 
https://github.com/rschuetzler/latex-icis-template/

The first (icis.cls) is a fully compliant template for submitting anonymous drafts for review. 
The second (icisfinal.cls) is final submission template. 

The last version from Ryan Schuetzler was added to this repository by 14 of April 2016. 

Requires XeLatex and biber 
Use \autocite{} for parentheticals, -> was found (Baron et al. 1999)
Use \textcite{} for text citations -> Baron et al. (1999) found that  
But as biber was loaded with "natbib=true", we can also use the classical citep/citet (compatibility advantage)

Similar template was used for ICIS 2015 DC and conference paper. All went fine in terms of formatting. 


%%%%%%%% Regarding the original ICIS template %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 %  The template file contains specially formatted styles (e.g.,
% \texttt{Normal, Heading, Bullet, References, Title, Author, Affiliation}) that
% are designed to reduce the work in formatting your final submission. 


%%%%%%%%%%%%% PAGE SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ny submission that exceeds page length limits will be rejected without review.
Completed research papers must not exceed fourteen (14) single-spaced pages. The 14-page count includes all text, figures, tables and appendices. Note that this page count excludes the cover page, abstract, keywords and references. 



